from math import log
from random import randint

from coopera_server import units


class LevelMap(object):

    def __init__(self, level):
        self.level = level
        self.current_turn = 0
        self.unit_count = 0
        self.won = False
        self.lost = False
        # Store the turn when the game ended. Usefull the let the game continue
        # some turns before chaging level.
        self.end_turn = False
        # Store events of a turn (like player actions)
        self.turn_history = []
        # Store elements that are not positioned at the map anymore
        self.removed_elements = []
        self.initialize_map()

    def ended(self):
        '''
        Returns False if map didn't end, or the number of turns since it ended.
        '''
        if self.end_turn:
            return self.current_turn - self.end_turn
        else:
            return False

    def genId(self) -> str:
        '''
        Generate an id to be used by a new unit.
        '''
        return '%s%s' % (self.level, self.unit_count)

    def create_unit(self, unit, pos):
        unit = unit(self.genId())
        self.elements[pos] = self.elements.get(pos, []) + [unit]
        self.unit_count += 1
        return unit

    def create_rand_unit(self, unit):
        pos = self.rand_pos(skip_area=self.get_player_pos() + (2,))
        return self.create_unit(unit, pos)

    def initialize_map(self):
        self.elements = {}
        lvl = self.level

        self.size = min(5 + round(log(lvl, 1.6)), 21)
        # guarantee odd size
        if self.size % 2 == 0:
            self.size -= 1

        map_center = int(self.size/2)
        map_center = (map_center, map_center)

        # Player
        self.create_unit(units.Player, map_center)

        # Capturable
        for i in range(round(log(lvl+1, 1.7))):
            self.create_rand_unit(units.Capturable)

        # Dangerous
        for i in range(round(log(lvl, 1.6))):
            self.create_rand_unit(units.Dangerous)

        # Wanderer
        for i in range(round(log(lvl, 4))):
            self.create_rand_unit(units.Wanderer)

        # Hunter
        if lvl > 5:
            for i in range(round(log(lvl-5, 3))):
                self.create_rand_unit(units.Hunter)

        # # Capturable (lvl1)
        # for i in range(round(log(lvl+1, 1.7))):
        #     self.create_rand_unit(units.Capturable)

        # # Dangerous (lvl2)
        # for i in range(round(log(lvl, 1.4))):
        #     self.create_rand_unit(units.Dangerous)

        # # Wanderer (lvl3)
        # for i in range(round(log(lvl, 1.6)-1)):
        #     self.create_rand_unit(units.Wanderer)

        # # Hunter (lvl5)
        # for i in range(round(log(lvl, 1.4)-4)):
        #     self.create_rand_unit(units.Hunter)

    def check_won(self) -> bool:
        for elements in self.elements.values():
            for element in elements:
                if element.denies_victory:
                    return False
        return True

    def check_lost(self) -> bool:
        for elements in self.elements.values():
            for element in elements:
                if element.forces_defeat:
                    return True
        return False

    def list_pos_units(self):
        '''Lists active units and their positions'''
        elements_list = []
        for pos, elements in self.elements.items():
            for element in elements:
                # Player must be the first, so it always plays before dangerous
                if isinstance(element, units.Player):
                    elements_list.insert(0, (pos, element))
                else:
                    elements_list.append((pos, element))
        return elements_list

    def run_turn(self, players: dict):
        '''
        Run a turn in the map, making all elements act.
        '''
        self.current_turn += 1
        self.turn_history = []

        for pos, element in self.list_pos_units():
            element.run({
                'pos': pos,
                'map': self,
                'players': players,
            })

        # end game checks
        if not self.end_turn:
            self.won = self.check_won()
            if not self.won:
                self.lost = self.check_lost()
            if self.won or self.lost:
                self.end_turn = self.current_turn

    def get_player_pos(self) -> tuple:
        for pos, elements in self.elements.items():
            for element in elements:
                if isinstance(element, units.Player):
                    return pos
        raise

    def remove_element_from_pos(self, element, pos):
        old_pos_list = self.elements[pos]
        old_pos_list.remove(element)
        if not old_pos_list:
            self.elements.pop(pos)

    def add_element_to_pos(self, element, pos):
        self.elements[pos] = self.elements.get(pos, []) + [element]

    def get_pos_for_element(self, element):
        for pos, _element in self.list_pos_units():
            if _element == element:
                return pos
        raise

    def move(self, a: tuple, b: tuple, element):
        ''' Move one element from a to b. '''
        self.remove_element_from_pos(element, a)
        self.add_element_to_pos(element, b)

    def add_history(self, event_type, extra_data={}):
        entry = {
            'type': event_type,
        }
        entry.update(extra_data)
        self.turn_history.append(entry)

    def touch_pos(self, pos, touching_unit):
        '''
        Run the touched method of all units in `pos`, saying that the
        `touching_unit` touched them.
        Returns True if any unit was touched, False otherwise.
        '''
        units_in_pos = self.elements.get(pos)
        if units_in_pos:
            for unit in units_in_pos:
                unit.touched(touching_unit, self)
            return True
        else:
            return False

    def remove_element(self, element):
        '''Removes one element from the map, but adds it to the removed list'''
        pos = self.get_pos_for_element(element)
        self.remove_element_from_pos(element, pos)
        self.removed_elements.append((pos, element))
        element.removed = True

    def get_units_pos(self, pos):
        return self.elements.get(pos)

    def check_passable_pos(self, pos):
        units_in_pos = self.elements.get(pos)
        if units_in_pos:
            for unit in units_in_pos:
                if unit.impassable:
                    return False
        return True

    def is_valid_pos(self, a: tuple, passable=False) -> bool:
        '''
        Check if a position is inside map. Can also check if is passable.
        '''
        return all((
            # map limits
            a[0] >= 0, a[0] < self.size, a[1] >= 0, a[1] < self.size,
            # no other impassable unit in position
            self.check_passable_pos(a) if passable else True
        ))

    def to_json(self) -> dict:
        return {
            'size': self.size,
            'elements': sorted([
                {**{'pos': pos}, **unit.to_json()}
                for pos, unit in self.list_pos_units()+self.removed_elements
            ], key=lambda x: x['id']),
            'currentTurn': self.current_turn,
            'won': self.won,
            'lost': self.lost,
            'history': self.turn_history
        }

    def rand_pos(self, area=None, skip_area=None, empty=True, passable=True,
                 tries=20) -> tuple:
        '''
        Returns a random position.
        Area limits the possible positions to an area. Should be a tuple with 3
        values: centerX, centerY, radius.
        Skip area avoids placing unit inside this area. Should be a tuple like
        above.
        Empty forces the position to be empty.
        Passable forces the position to be passable.
        Tries limits the number of tries to generate a valid position.
        '''
        pos = None
        _max = self.size - 1
        count = 0
        while not pos:
            # limit check
            count += 1
            if count > tries:
                raise

            if area:
                # enforce area
                cx, cy, r = area
                pos = (
                    randint(max(cx-r, 0), min(cx+r, _max)),
                    randint(max(cy-r, 0), min(cy+r, _max)))
            else:
                pos = (randint(0, _max), randint(0, _max))

            # skip area check
            if skip_area:
                cx, cy, r = skip_area
                x, y = pos
                if x > cx-r and x < cx+r and y > cy-r and y < cy+r:
                    pos = None
                    continue

            units_in_pos = self.elements.get(pos)
            if units_in_pos:
                # empty check
                if empty:
                    pos = None
                # passable check
                elif passable and not self.check_passable_pos(pos):
                    pos = None

        return pos


def generate_map(level: int) -> LevelMap:
    '''
    Generate a level map based on a level value.
    '''
    return LevelMap(level)
